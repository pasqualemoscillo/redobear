//
//  CoreDataController.swift
//  redoBear
//
//  Created by Pasquale Moscillo on 09/12/2019.
//  Copyright © 2019 Pasquale Moscillo. All rights reserved.
//

import Foundation
import CoreData
import UIKit


class CoreDataController {
  
    static let shared = CoreDataController()
    
    private var context: NSManagedObjectContext
    
    private init() {
        let application = UIApplication.shared.delegate as! AppDelegate
        self.context = application.persistentContainer.viewContext
    }
    
    func addNote(textNote: String, dateNote: Date) {
        let entity = NSEntityDescription.entity(forEntityName: "newNote", in: self.context)
        let newNote = DataNotes(entity: entity!, insertInto: self.context)
        
        newNote.dateNote = dateNote
        newNote.textNote = textNote
        
        do {
            try self.context.save()
        } catch let error {
            print("impossible to save \(newNote)")
        }
        print("\(newNote) printed correctly")
    }
}
