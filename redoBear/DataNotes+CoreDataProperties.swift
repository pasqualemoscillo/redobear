//
//  DataNotes+CoreDataProperties.swift
//  redoBear
//
//  Created by Pasquale Moscillo on 09/12/2019.
//  Copyright © 2019 Pasquale Moscillo. All rights reserved.
//
//

import Foundation
import CoreData


extension DataNotes {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DataNotes> {
        return NSFetchRequest<DataNotes>(entityName: "DataNotes")
    }

    @NSManaged public var dateNote: Date?
    @NSManaged public var textNote: String?


    
}
