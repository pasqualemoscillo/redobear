//
//  NoteTableViewCell.swift
//  redoBear
//
//  Created by Pasquale Moscillo on 05/12/2019.
//  Copyright © 2019 Pasquale Moscillo. All rights reserved.
//

import UIKit
import CoreData

class NoteTableViewCell: UITableViewCell {

   
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var notePreviewLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        dateLabel.text = "\(Date(timeIntervalSinceNow: 0.0))"
        notePreviewLabel.text = "fhjdsonfndkjlwnfknerlwnfjewnlf"
        
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
